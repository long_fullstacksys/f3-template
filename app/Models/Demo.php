<?php
/**
 *
 * @Project: itap
 * @Filename: Demo.php
 * @Author: longnc <nguyenchilong90@gmail.com>
 * @Created Date: 4/7/21 2:22 AM
 *
 * @Description: Text description here
 */

namespace App\Models;


class Demo extends Model
{
	public function __construct()
	{
		parent::__construct('tblbanner');
	}

	/**
	 * get all
	 * @return Demo[]
	 */
	public function getAll() {
		return $this->find();
	}
}
