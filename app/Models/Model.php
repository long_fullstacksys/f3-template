<?php


namespace App\Models;

use DB\SQL;

class Model
{
	protected $db;
	public function __construct()
	{
		$dbHost = $_ENV('DB_HOST');
		$dbPort = $_ENV('DB_PORT');
		$dbUser = $_ENV('DB_USER');
		$dbPassword = $_ENV('DB_PASS');
		$dbName = $_ENV('DB_NAME');
		$dsn = "mysql:host={$dbHost};port={$dbPort};dbname={$dbName}";
		$this->db = new SQL(
			$dsn,
			$dbUser,
			$dbPassword,
			array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
		);
	}

}
