<?php

namespace App\Controllers;

use Template;
use App\Models\Demo;

class Home extends Controller {

	private $model;
	public function __construct()
	{
		$this->model = new Demo();
		parent::__construct();
	}

	public function index () {
		$this->f3->set('key', 'value');
		$this->f3->set('data', $this->model->getAll());
		echo Template::instance()->render('home.htm');
	}
}
