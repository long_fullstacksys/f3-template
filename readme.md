# Fat Free Framework Template
This is a simple boilerplate for Projects using the [Fat-Free Framework](https://fatfreeframework.com/).

## Getting Started
- With `composer create-project`
	- Replace `my-project` with your project name:
	```console
	composer create-project long_fullstacksys/f3-template my-project
	```
- With `composer install`
	- Clone this repository and then run:
	```console
	composer install
	```
	- Then copy `.env.example` to `.env`

## Local setup
- Required install `PHP/Apche|Nginx/Mysql` and `composer`
- Clone source to local then move to folder with command `cd path_root_folder`
- Run `composer install` to install php packages
- Then copy `.env.example` to `.env`
- Update db config at `.env` file then start check on browser now
- Create Symbolic Link for `uploads` folder can use in browser via command
	```shell
	cd root_folder_project
	ln -s {root_folder_project}/storage/uploads {root_folder_project}/public/uploads
	
	# For windows (PowerShell)
	# open PowerShell with [Run as Administrator]
	mklink /D {root_folder_project}/storage/uploads {root_folder_project}/public/uploads
	```
	after run command then check the same image bellow ![Create Symbolic Link](public/assets/images/create-symbolic-link.png "Create Symbolic Link")

## For create new controller/model/etc
- Can check simple code demo for new structures
  - Controller Base [Here](app/Controllers/Controller.php) [Demo Controller](app/Controllers/Home.php)
  - Model Base [Here](app/Models/Model.php) and [Demo Model](app/Models/Demo.php)
- All Classes in `app` folder will autoload with composer and each folder is a namespace
  - With `Controller` then is `namespace App\Controllers;`
  - With `Model` then is `namespace App\Models;`
  - With `Helper` then is `namespace App\Helpers;`
  - With `Services` then is `namespace App\Services;`
- Review [Code Flow](codeflow.md)
- Review [Git Flow](gitflow.md)

## Branch coding
- Only checkout from develop branch then start code branch just create then make Pull Request `PR` after done code and wait merge
- Branch name format `task-jira-id_short_description`, example `ADEV-45_create_form_login`

## Directory Structure
- The directory structure is heavily inspired by Laravel/Lumen and can be customized however you want:
```
.
|-- .env               # environment variables
|-- app/
│   |-- Controllers/
│   |-- Models/
│   |── Helpers/       # helper functions and classes
|   |-- Services/
|-- config/
│   |-- bootstrap.php  # initializes the whole application
|   |-- db.php         # db config
│   |-- globals.php    # framework variables and other globals
│   |-- routes.php     # routes, maps and redirects
|-- public/            # public web root
│   |-- index.php      # entry point of the whole application
|-- resources/
│   |-- langs/         # localization files
│   |-- views/         # views/layouts
|-- storage/           # storage for the application (needs chmod 0777)
│   |-- cache/
│   |-- logs/ 
│   |-- tmp/           # temporary files  
│   |-- uploads/
|-- vendor/            # composer install directory
|-- tests/             # unittest
```

## Recommend For Dev
- Recommend use [PHPStorm](https://www.jetbrains.com/phpstorm/) for IDE coding and [xdebug](https://xdebug.org/) for step-by-step 
  debugging on localhost
- Use [xdebug in PHPStorm](https://www.jetbrains.com/help/phpstorm/configuring-xdebug.html)
- Recommend Dev read about [PHP Standard](https://www.php-fig.org/psr/) and focus on [PSR-12](https://www.php-fig.org/psr/psr-12)
- Recommend Dev read about [PHPCS Tool](https://github.com/squizlabs/PHP_CodeSniffer) and how to use with PHPStorm 
  at [PHPStorm & PHPCS](https://www.jetbrains.com/help/phpstorm/using-php-code-sniffer.html)
- Recommend Dev use MacOS/Linux(ubuntu, lubuntu, etc) and Docker for develop, if use Windows then can use Docker or 
  [Vagrant by HashiCorp](https://www.vagrantup.com/)  
- With F3 (Fat Free Framework) then please use built-in ORM with `SQL/Mapper` and don't using `RedBeanPHP` or 
  another for external ORM