<?php
/**
 *
 * @Project: itap
 * @Filename: TestBase.php
 * @Author: longnc <nguyenchilong90@gmail.com>
 * @Created Date: 4/6/21 10:41 PM
 *
 * @link https://fatfreeframework.com/3.7/unit-testing
 * @link https://fatfreeframework.com/3.7/test
 */

namespace Tests;


class TestBase
{

	protected $f3Test;

	public function __construct()
	{
		$this->f3Test = new \Test();
	}

}
