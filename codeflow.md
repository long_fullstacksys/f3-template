# Simple Code for Working
- Follow simple code for create new file/class for do task
- On Controller Base [Here](app/Controllers/Controller.php) then can write more functions for common use in each project
- On Model Base [Here](app/Models/Model.php) can write common query and not need use external ORM on F3
  - Can write getAll, getOne, update, insert, etc
  - Can write more functions here to apply for global Model in project	
- Can check simple code demo for new structures
	- Controller Base [Here](app/Controllers/Controller.php) [Demo Controller](app/Controllers/Home.php)
	- Model Base [Here](app/Models/Model.php) and [Demo Model](app/Models/Demo.php)
- All Classes in `app` folder will autoload with composer and each folder is a namespace
	- With `Controller` then is `namespace App\Controllers;`
	- With `Model` then is `namespace App\Models;`
	- With `Helper` then is `namespace App\Helpers;`
	- With `Services` then is `namespace App\Services;`
## Recommend
- Recommend use `nginx` instead `apache2` for webserver
- Recommend use `Docker & Docker Compose` on local dev
- Refers docs for Docker
  - [docker](https://docs.docker.com/engine/)
  - [docker compose](https://docs.docker.com/compose/)
  - [docker for developers](https://dev.to/pavanbelagatti/getting-started-with-docker-for-developers-3apo)
  - [simple use docker](https://github.com/PacktPublishing/Docker-for-Developers)
- Recommend use [PHPStorm](https://www.jetbrains.com/phpstorm/) for IDE coding and [xdebug](https://xdebug.org/) for step-by-step
  debugging on localhost
- Use [xdebug in PHPStorm](https://www.jetbrains.com/help/phpstorm/configuring-xdebug.html)
- Recommend Dev read about [PHP Standard](https://www.php-fig.org/psr/) and focus on [PSR-12](https://www.php-fig.org/psr/psr-12)
- Recommend Dev read about [PHPCS Tool](https://github.com/squizlabs/PHP_CodeSniffer) and how to use with PHPStorm
  at [PHPStorm & PHPCS](https://www.jetbrains.com/help/phpstorm/using-php-code-sniffer.html)
- Recommend Dev use MacOS/Linux(ubuntu, lubuntu, etc) and Docker for develop, if use Windows then can use Docker or
  [Vagrant by HashiCorp](https://www.vagrantup.com/)
- With F3 (Fat Free Framework) then please use built-in ORM with `SQL/Mapper` and don't using `RedBeanPHP` or
  another for external ORM
