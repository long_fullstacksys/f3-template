<?php

/**
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
*/
require_once ROOT . '/vendor/autoload.php';

/**
|--------------------------------------------------------------------------
| Bring in (env)
|--------------------------------------------------------------------------
|
| Quickly use our environment variables
|
*/

$dotenv = \Dotenv\Dotenv::createImmutable(ROOT);
$dotenv->load();


/*
|--------------------------------------------------------------------------
| Get application instance
|--------------------------------------------------------------------------
|
| Here we will get the application instance that serves as
| the central piece of this framework.
|
*/

$f3 = \Base::instance();


/**
|--------------------------------------------------------------------------
| Load Config File
|--------------------------------------------------------------------------
|
| Now we will load the the "globals" configuration file, which
| contains various framework configuration for Fat-Free.
|
*/

require __DIR__ . '/globals.php';


/**
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes config file so that they can all
| be added to the application. This will provide all of the URLs
| the application can respond to, as well as the controllers
| that may handle them.
|
*/

require __DIR__ . '/routes.php';

return $f3;
