<?php

/*
|--------------------------------------------------------------------------
| Get application instance
|--------------------------------------------------------------------------
|
| Here we will get the application instance that serves as
| the central piece of this framework.
|
*/

$f3 = \Base::instance();



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Fat-Free the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$f3->route('GET /','Controllers\Home->index');


/*
GET     /                                                =   HomeController->index
GET     /@page                                           =   StaticController->index
POST    /@page                                           =   StaticController->index

GET      /supporting-media                               =   FSupportingMediaController->index
GET      /supporting-organisations                       =   FSupportingOrganisationController->index

;===================================Back-End==============================================================

GET      /admin                                          =   AdminController->home
POST     /admin/login                                    =   LoginController->index
GET      /logout                                         =   LoginController->logout

GET      /admin/highlights                               =   HighlightsListController->index
GET      /admin/highlights/edit/@id                      =   HighlightsController->edit
GET      /admin/highlights/edit                          =   HighlightsController->edit
POST     /admin/highlights                               =   HighlightsController->storeHighlights
PUT      /admin/highlights                               =   HighlightsController->storeHighlights
GET      /admin/highlights/@id                           =   HighlightsController->getId
DELETE   /admin/highlights/@id                           =   HighlightsController->delete

;==================================Start Users===========================================================
GET      /admin/user                                     = UserListController->index
GET      /admin/user/edit                                = UserController->edit
GET      /admin/user/edit/@id                            = UserController->edit
POST|PUT /admin/user                                     = UserController->storeUser
PUT      /admin/user/access                              = UserController->access
DELETE   /admin/user/@id                                 = UserController->delete
;======================================End User=========================================================

GET      /admin/profile/edit/@id                         =   ProfileController->editProfile
POST     /admin/profile                                  =   ProfileController->storeProfile

GET      /admin/slider                                   =   SliderListController->index
GET      /admin/slider/edit/@id                          =   SliderController->edit
GET      /admin/slider/edit                              =   SliderController->edit
POST|PUT /admin/slider                                   =   SliderController->storeSlider
GET      /admin/slider/@id                               =   SliderController->getId
POST     /admin/slider/@id                               =   SliderController->delete

GET      /admin/currentjob                               =   CurrentJobListController->index
GET      /admin/currentjob/edit/@id                      =   CurrentJobController->edit
GET      /admin/currentjob/edit                          =   CurrentJobController->edit
POST|PUT /admin/currentjob                               =   CurrentJobController->storeCurrentJob
GET      /admin/currentjob/@id                           =   CurrentJobController->getId
DELETE   /admin/currentjob/@id                           =   CurrentJobController->delete

GET      /admin/news-and-resources                       =   NewsAndResourcesListController->index
GET      /admin/news-and-resources/edit/@id              =   NewsAndResourcesController->edit
GET      /admin/news-and-resources/edit                  =   NewsAndResourcesController->edit
POST     /admin/news-and-resources                       =   NewsAndResourcesController->storeNewsAndResources
PUT      /admin/news-and-resources                       =   NewsAndResourcesController->storeNewsAndResources
GET      /admin/news-and-resources/@id                   =   NewsAndResourcesController->getId
DELETE   /admin/news-and-resources/@id                   =   NewsAndResourcesController->delete

GET      /admin/webpage                                  =   WebPageListController->index
GET      /admin/webpage/edit/@id                         =   WebPageController->edit
GET      /admin/webpage/edit                             =   WebPageController->edit
POST|PUT /admin/webpage                                  =   WebPageController->storeWebPage
GET      /admin/webpage/@id                              =   WebPageController->getId
DELETE   /admin/webpage/@id                              =   WebPageController->delete

;==================================Navigation=====================================================
GET      /admin/navigation                               = NavigationListController->index
GET      /admin/navigation/edit                          = NavigationController->edit
GET      /admin/navigation/edit/@id                      = NavigationController->edit
POST|PUT /admin/navigation                               = NavigationController->storeNavigation
GET      /admin/navigation/@id                           = NavigationController->getId
DELETE   /admin/navigation/@id                           = NavigationController->delete
PUT      /admin/navigation/publish                       = NavigationController->publish
;==================================Itapconnect=======================================================
GET      /admin/itapconnect                              =   ItapConnectListController->index
GET      /admin/itapconnect/edit/@id                     =   ItapConnectController->edit
GET      /admin/itapconnect/edit                         =   ItapConnectController->edit
POST|PUT /admin/itapconnect                              =   ItapConnectController->storeItapConntect
GET      /admin/itapconnect/@id                          =   ItapConnectController->getId
DELETE   /admin/itapconnect/@id                          =   ItapConnectController->delete
PUT      /admin/itapconnect/publish                      =   ItapConnectController->publish

;==================================Speakers=======================================================
GET      /admin/speaker                                  =   SpeakersListController->index
GET      /admin/speaker/edit/@id                         =   SpeakersController->edit
GET      /admin/speaker/edit                             =   SpeakersController->edit
POST|PUT /admin/speaker                                  =   SpeakersController->storeSpeakers
GET      /admin/speaker/@id                              =   SpeakersController->getId
DELETE   /admin/speaker/@id                              =   SpeakersController->delete
POST     /admin/speaker/ordernum                         =   SpeakersController->ordernum

;==================================Pressrelease=======================================================
GET      /admin/pressrelease                             =   PressReleaseListController->index
GET      /admin/pressrelease/edit/@id                    =   PressReleaseController->edit
GET      /admin/pressrelease/edit                        =   PressReleaseController->edit
POST|PUT /admin/pressrelease                             =   PressReleaseController->storePressRelease
GET      /admin/pressrelease/@id                         =   PressReleaseController->getId
DELETE   /admin/pressrelease/@id                         =   PressReleaseController->delete
POST     /admin/pressreleaseordernum                     =   PressReleaseController->ordernum

;==================================Photo Gallery=======================================================
GET      /admin/photogallery                             =   PhotoGalleryListController->index
GET      /admin/photogallery/edit/@id                    =   PhotoGalleryController->edit
GET      /admin/photogallery/edit                        =   PhotoGalleryController->edit
POST|PUT /admin/photogallery                             =   PhotoGalleryController->storePhotogallery
GET      /admin/photogallery/@id                         =   PhotoGalleryController->getId
DELETE   /admin/photogallery/@id                         =   PhotoGalleryController->delete
POST     /admin/photogalleryordernum                     =   PhotoGalleryController->ordernum

;==================================Video Gallery=======================================================
GET      /admin/videogallery                             =   VideoGalleryListController->index
GET      /admin/videogallery/edit/@id                    =   VideoGalleryController->edit
GET      /admin/videogallery/edit                        =   VideoGalleryController->edit
POST|PUT /admin/videogallery                             =   VideoGalleryController->storeVideogallery
GET      /admin/videogallery/@id                         =   VideoGalleryController->getId
DELETE   /admin/videogallery/@id                         =   VideoGalleryController->delete
POST     /admin/videogallery/ordernum                    =   VideoGalleryController->ordernum

;==================================Start Banner=========================================================
GET      /admin/banner                                   = BannerListController->index
GET      /admin/banner/edit                              = BannerController->edit
GET      /admin/banner/edit/@id                          = BannerController->edit
POST|PUT /admin/banner                                   = BannerController->storeBanner
PUT      /admin/banner/access                            = BannerController->access
DELETE   /admin/banner/@id                               = BannerController->delete
PUT      /admin/banner/order-num                         = BannerController->orderNum
;======================================End Banner=======================================================

GET      /admin/salesrepresentative                      =   SalesRepresentativeListController->index
GET      /admin/salesrepresentative/edit/@id             =   SalesRepresentativeController->edit
GET      /admin/salesrepresentative/edit                 =   SalesRepresentativeController->edit
POST|PUT /admin/salesrepresentative                      =   SalesRepresentativeController->storeSalesrepresentative
GET      /admin/salesrepresentative/@id                  =   SalesRepresentativeController->getId
DELETE   /admin/salesrepresentative/@id                  =   SalesRepresentativeController->delete
PUT      /admin/salesrepresentative/publish              =   SalesRepresentativeController->publish

GET      /admin/exhibitors                               =   ExhibitorsListController->index
GET      /admin/exhibitors/edit/@id                      =   ExhibitorsController->edit
GET      /admin/exhibitors/edit                          =   ExhibitorsController->edit
POST|PUT /admin/exhibitors                               =   ExhibitorsController->storeExhibitors
GET      /admin/exhibitors/@id                           =   ExhibitorsController->getId
DELETE   /admin/exhibitors/@id                           =   ExhibitorsController->delete

GET      /admin/conference                               =   ConferenceListController->index
GET      /admin/conference/edit/@id                      =   ConferenceController->edit
GET      /admin/conference/edit                          =   ConferenceController->edit
POST|PUT /admin/conference                               =   ConferenceController->storeConference
GET      /admin/conference/@id                           =   ConferenceController->getId
DELETE   /admin/conference/@id                           =   ConferenceController->delete
PUT      /admin/conference/publish                       =   ConferenceController->publish
POST     /admin/copy-speaker                             =   SpeakersController->storeSpeakerCopy
;=====================================Book a stand=========================================================
GET      /admin/book-a-stand                             =   BookAstandListController->index
GET      /admin/book-a-stand/export                      =   ExportController->BookAStand
;=====================================Join mailing========================================================
GET      /admin/join-mailing                             =   JoinMailingListController->index
GET      /admin/join-mailing/export                      =   ExportController->JoinMailing
;=====================================2019 post show report===============================================
GET      /admin/2019-post-show-report                    =   PostShowReportController->index
GET      /admin/2019postshowreport/export                =   ExportController->PostShowReport
;=====================================News Letter==========================================================
GET      /admin/newsletter                               =   NewsLetterController->index
GET      /admin/newsletter/export                        =   ExportController->NewsLetter
;=====================================Download Salesbrochure===============================================
GET      /admin/download-sales-brochure                  =   DownloadSalesBrochureController->index
GET      /admin/download-sales-brochure/export           =   ExportController->DownloadSalesBrochure
;=====================================Application Form=====================================================
GET      /admin/application-form                         =   ApplicationFormController->index
GET      /admin/application-form/export                  =   ExportController->ApplicationForm
GET      /admin/application-form/@id                     =   ApplicationFormController->index
;=====================================Websote Content======================================================
GET      /admin/content                                  =   ContentController->index
GET      /admin/content/@name                            =   ContentController->content
POST     /admin/content                                  =   ContentController->storeContent
GET      /admin/content/edit/@id                         =   ContentController->edit
;=======================================Forum======================================================
GET      /admin/forum                                    =   ForumlistController->index
GET      /admin/forum/edit/@id                           =   ForumController->edit
GET      /admin/forum/edit                               =   ForumController->edit
POST|PUT /admin/forum                                    =   ForumController->storeForum
GET      /admin/forum/@id                                =   ForumController->getId
DELETE   /admin/forum/@id                                =   ForumController->delete
PUT      /admin/forum/publish                            =   ForumController->publish
;=======================================Forum======================================================
GET      /admin/committee                                =   CommitteeListController->index
GET      /admin/committee/edit/@id                       =   CommitteeController->edit
GET      /admin/committee/edit                           =   CommitteeController->edit
POST|PUT /admin/committee                                =   CommitteeController->storeCommittee
GET      /admin/committee/@id                            =   CommitteeController->getId
DELETE   /admin/committee/@id                            =   CommitteeController->delete
POST     /admin/committeesordernum                       =   CommitteeController->ordernum
;=======================================Sponsors=====================================================
GET      /admin/sponsors                                 =   SponsorsListController->index
GET      /admin/sponsors/edit/@id                        =   SponsorsController->edit
GET      /admin/sponsors/edit                            =   SponsorsController->edit
POST|PUT /admin/sponsors                                 =   SponsorsController->storeSponsors
GET      /admin/sponsors/@id                             =   SponsorsController->getId
DELETE   /admin/sponsors/@id                             =   SponsorsController->delete
POST     /admin/sponsorsordernum                         =   SponsorsController->ordernum
;=======================================Supportingmedia=====================================================
GET      /admin/supportingmedia                          =   SupportingMediaListController->index
GET      /admin/supportingmedia/edit/@id                 =   SupportingMediaController->edit
GET      /admin/supportingmedia/edit                     =   SupportingMediaController->edit
POST|PUT /admin/supportingmedia                          =   SupportingMediaController->storeSupportingMedia
GET      /admin/supportingmedia/@id                      =   SupportingMediaController->getId
DELETE   /admin/supportingmedia/@id                      =   SupportingMediaController->delete
POST     /admin/supportingmediaordernum                  =   SupportingMediaController->ordernum
;=======================================supportingorgOrg=====================================================
GET      /admin/supportingorg                            =   SupportingOrgListController->index
GET      /admin/supportingorg/edit/@id                   =   SupportingOrgController->edit
GET      /admin/supportingorg/edit                       =   SupportingOrgController->edit
POST|PUT /admin/supportingorg                            =   SupportingOrgController->storeSupportingOrg
GET      /admin/supportingorg/@id                        =   SupportingOrgController->getId
DELETE   /admin/supportingorg/@id                        =   SupportingOrgController->delete
POST     /admin/supportingorgordernum                    =   SupportingOrgController->ordernum
;==================================Navigation=====================================================
GET      /admin/footer-navigation                               = FooterNavigationListController->index
GET      /admin/footer-navigation/edit                          = FooterNavigationController->edit
GET      /admin/footer-navigation/edit/@id                      = FooterNavigationController->edit
POST|PUT /admin/footer-navigation                               = FooterNavigationController->storeNavigation
GET      /admin/footer-navigation/@id                           = FooterNavigationController->getId
DELETE   /admin/footer-navigation/@id                           = FooterNavigationController->delete
PUT      /admin/footer-navigation/publish                       = FooterNavigationController->publish
*/
