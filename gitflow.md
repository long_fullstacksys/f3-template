# Simple GitFlow
- `NOT MAKE CHANGE DIRECTLY ON MASTER/MAIN/DEVELOP` branch and much always checkout new branch then do anything here 
  and can push & make Pull Request `PR` or delete it
- `NOT EDIT CODE DIRECTLY ON FTP/SERVER` much always edit/update/change from local by new branch then push to git
- Only checkout from `develop` branch then create new branch then make `PR` after done code
- Branch name format `task-jira-id_short_description`, example `ADEV-45_create_form_login`
- Prefix for checkout branch
  - `feature/` use for create normal task on project
  - `release/` only use for release version or phases of project, it's same tag and release but more simple
  - `hotfix/` Only use when need fix bugs on server, live/production and priority highest
- After done task then ping Lead to review code, recommend push daily to easy review code and fix faster
- One branch can make multiple commit and always keep branch without delete after merge success
- One commit code please write `message clear, full description` of task or this commit, not write very short, not 
  meaning
- If on current PC already use multiple git accounts then please remember run bellow command at clone source
```shell
# after clone source to local
cd path_to_source_just_clone
git config user.name "Your Name"
git config user.email "email_using_login_this_repo"
```
then check again with command `vi .git/config` and see ![Config Git Local](public/assets/images/config_git_local.png)
- Can use [Source Tree](https://www.sourcetreeapp.com/) / [GitKraken](https://www.gitkraken.com/) / [Other Git GUI](https://git-scm.com/downloads/guis) 
  for easy working with GIT on local, but recommend use terminal
