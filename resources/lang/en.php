<?php

return [
	'love' => 'The key without param => I love F3',
	'today' => 'The key include param => Today is {0, date}',
	'pi' => 'The key include param => The Pi number {0, number}',
	'money' => 'The key include param => Amount remaining: {0, number, currency}',
	'txt_copyright' => 'Copyright'
];
